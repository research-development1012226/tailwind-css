import React from 'react';
const Card = ({ title, description, imageUrl, tag}) => {
    return (
        <div className="max-w-sm rounded overflow-hidden shadow-lg bg-custom-white">
            <img className="w-full" src={imageUrl} alt={title}/>
            <div className="px-6 py-4">
                <div className="font-bold text-xl mb-2">{title}</div>
                <p className="text-custom-gray text-base">{description}</p>
            </div>
            <div className="px-6 py-4">
                <span className="inline-block bg-custom-blue rounded-full px-3 py-1
                text-sm font-normal text-custom-white mr-2">{tag}</span>
            </div>
        </div>
    );
};

export default Card;



