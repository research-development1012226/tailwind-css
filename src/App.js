import "./App.css";
import TestTailwind from "./TestTailwind";

function App() {
  return (
    <div className="App">
      <div className="bg-red-500 flex flex-col items-center justify-center h-screen">
        <h1 className="text-bold text-center text-violet-600 grid justify-center items-center">
          Hello World
        </h1>
      </div>
      <TestTailwind />
    </div>
  );
}

export default App;
