import React from 'react'
import "./index.css"
import Card from './Card'
const TestTailwind = () => {
    return (
        <>
            <div className="flex justify-center items-center h-screen bg-custom-gray">
                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
                    <Card
                        title="Kilo IT"
                        description="Work less, Worry Less, Succeed More"
                        imageUrl="https://shorturl.at/1LY2q"
                        tag='Information Technology'
                    />
                    <Card
                        title="Kilo Health"
                        description="Our health is the most import in our life."
                        imageUrl="https://shorturl.at/zyDYP"
                        tag='Healthy Life'
                    />
                    <Card
                        title="Kilo Travel"
                        description="Work life balance, take time to travel."
                        imageUrl="https://shorturl.at/x7G29"
                        tag='Travel Life'
                    />
                </div>
            </div>
        </>
    )
}
export default TestTailwind;

