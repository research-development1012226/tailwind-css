module.exports = {
  theme: {
    extend: {
      colors: {
        "custom-blue": "#405cff",
        "custom-gray": "#6b7280",
        "custom-white": "#eaf3ff",
      },
      spacing: {
        128: "32rem",
        144: "36rem",
      },
      fontFamily: {
        sans: ["Montserrat"],
      },
      lineHeight: {
        sm: "1.25",
        base: "1.5",
        lg: "1.75",
      },
      fontWeight: {
        normal: "400",
        medium: "500",
        bold: "700",
      },
    },
  },
  plugins: [
    require("@tailwindcss/forms"),
    require("@tailwindcss/aspect-ratio"),
    require("@tailwindcss/typography"),
  ],
  purge: {
    enabled: process.env.NODE_ENV === "production",
    content: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  },
};
